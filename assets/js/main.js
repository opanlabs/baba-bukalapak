$(document).ready(function(){

	$(".dropdown-menu li a").click(function(){
	  $(".btn:first-child").html('<img src="assets/img/ico-flag.png" alt="" class="img-responsive"> <span class="txt">' + $(this).text()+'</span> <span class="caret"></span>');
	});

	// dropdown dropdown-menu
	$(".notif").click(function(){
	   $(".dropdown-menux").toggle();
	   $(".dropdown-menux2").hide();
	   $("#collapseExample").hide();
	});
	$(".cart").click(function(){
	   $(".dropdown-menux2").toggle();
	   $(".dropdown-menux").hide();
	   $("#collapseExample").hide();
	});
	$(".sign a").click(function(){
	   $("#collapseExample").toggle();
	   $(".dropdown-menux").hide();
	   $(".dropdown-menux2").hide();
	});

	// banner slide
	$('.slidebanner').slick({
		dots:false,
		slidesToShow: 2,
	  	slidesToScroll: 1,
	  	variableWidth: true
	});

	// slide product
	$('.slide-product').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

});